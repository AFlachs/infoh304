#ifdef OSX
#include <GLUT/glut.h>
#elif LINUX
#include <GL/glut.h>
#endif
#include <iostream>
#include <boost/functional/hash.hpp>
#include <string>
#include <set>
#include <utility>
#include <random>
#include <fstream>

using namespace std;

set<string> words;
GLint* points;
int size = 0;

void init(float r, float g, float b)
{
	glClearColor(r,g,b,0.0);  
	glMatrixMode (GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D (1.0, 128.0, 1.0, 128.0);
}

unsigned long long prehash_function(string word) {
  /*
   * Une lettre est sur 7 bits (entre 0 et 127) donc on va sommer toutes les valeurs
   * des lettres asciis avec à chaque fois un décalage de 7 bits (donc multiplier par 2^7)
   */
  int word_len = word.size();
  unsigned long long res = 0;

  for (int i=0; i<word_len; i++) {
    unsigned long long cur_char = word[i];     
    res += (unsigned long long) pow(2,7*i)*cur_char;
  }
  return res;
}


pair<int,int> division_hash_function(string word)
{
  /*
   * Pour hasher par division on va couper le mot en deux au milieu et préhasher chacun
   *
	int x, y;
	pair<int,int> coord;
  string half1, half2;
  half1 = word;

  for (int i=0; i<word.size()/2; i++) {
    half1.pop_back();
    half2.push_back(word[word.size()-i-1]);
  }

  unsigned long long pre_hashed_1 = prehash_function(half1);
  unsigned long long pre_hashed_2 = prehash_function(half2);
  
  x = pre_hashed_1 % 147 ; //147 est impair et loin d'une puissance de deux
  y = pre_hashed_2 % 147 ;
   *  Mauvaise idée parce qu'on ne "mélange" pas bien le mot donc on réduit l'aspet aléatoire de la distri.
  */ 
  int x, y;
  unsigned long long hash, prehash;
  pair<int,int> coord;

  // prehash
  prehash = prehash_function(word);

  int m = 9358217; // on se met loin d'une puissance de 2;
  hash = prehash % m; 

  // On veut répartir correctement x et y;
  // On a besoin de 14 bits "aléatoires" donc on va garder les 14 derniers
  // les 7 derniers sont trouvés via le modulo de 2^7 donc 128
  // les 7 précédants sont trouvés en "décalant" hash de 7 bits puis re modulo 128
  // le reste est trouvé par l'opération modulo

  x = hash % 128 +1; //on extrait les 7 premiers bits
  y = (hash/128)%128 +1; // +1 pour avoir entre 1 et 128 plutot que 0 et 127;
  // y = (hash >> 7) % 128 +1;
  
  coord = make_pair(x,y);
	return coord;
}

pair<int,int> hash_function(string word)
{
  boost::hash<string> hash;
  size_t hashage = hash(word);
  x = hash % 128 +1; //on extrait les 7 premiers bits
  y = (hash/128)%128 +1; // +1 pour avoir entre 1 et 128 plutot que 0 et 127;
  // y = (hash >> 7) % 128 +1;
	return make_pair(x,y);
}

void make_points()
{
	pair<int,int> coord;
	int i=0;
	for (set<string>::iterator it=words.begin(); it!=words.end(); ++it)
	{
		//coord = hash_function(*it);
    coord = division_hash_function(*it);
		points[i++] = (GLint)coord.first;
		points[i++] = (GLint)coord.second;	
	}
}
void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(1.0, 0.0, 0.0);
	glViewport(128.0,128.0,512.0,512.0);
	glPointSize(2.0f);
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(2, GL_INT, 0, points);
	glDrawArrays(GL_POINTS,0,size);
	glDisableClientState(GL_VERTEX_ARRAY);
	glutSwapBuffers();
}

int main(int argc,char *argv[])
{
	
	string word;

	if ( argc != 2 )
	{
		cout << "Enter a file name." << endl;
	}
	else
	{
		ifstream file (argv[1]);
		
		string str;
		while ( file >> word )
		{
			words.insert(word);
		}
		size = words.size();
		cout << "Document size: " << words.size() << " words" << endl;

		points = (GLint *)malloc(2*size*sizeof(GLint));
		make_points();
		glutInit(&argc,argv);
		glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);
		glutInitWindowSize (768, 768);
		glutInitWindowPosition (200, 200);
		glutCreateWindow ("Hash Function Visualization");
		init(0.0,0.0,0.0);
		glutDisplayFunc(display);
		glutMainLoop();
	}
	return 0;
}
