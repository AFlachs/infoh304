#ifdef OSX
#include <GLUT/glut.h>
#elif LINUX
#include <GL/glut.h>
#endif
#include <iostream>
#include <string>
#include <set>
#include <array>
#include <utility>
#include <random>
#include <fstream>
#include <sstream>
#include <climits>
#include <chrono>
#include <thread>
#include <iterator>

using namespace std;

array<int,81> sudoku, sudoku_initial; //Tableau unidimensionnel équivalent à un 9x9
	// sudoku_initial est la copie de travail.
const int TIMERMSECS = 40;
array<set<int>,27> conditions; //Toutes les fonctions d'autorisation du sudoku
	// sont déjà écrites 

void init(float r, float g, float b)
{
	glClearColor(r,g,b,0.0);
	glMatrixMode (GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D (0.0, 92.0, 0.0,110.0);
}

void draw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(0.0, 0.0, 0.0);
	glViewport(100.0,100.0,500.0,500.0);
	glPointSize(1.0f);
	
	//draw title
	stringstream ss1;
	ss1<<"Solving Sudoku Puzzle ";
	glRasterPos2i(27,110);
	for (int i=0; i<ss1.str().size(); i++)
		glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, ss1.str().at(i));
	
	//draw grid
	for(int i = 0; i <=9; i ++)
	{
		if(i%3==0)
			glLineWidth(2.0f);
		glBegin(GL_LINES);
		glVertex3f((float)i*10+1, 1.0f, 0.0f);
		glVertex3f((float)i*10+1, 91.0f, 0.0f);
		glVertex3f(1.0f, (float)i*10+1, 0.0f);
		glVertex3f(91.0f, (float)i*10+1, 0.0f);
		glEnd();
		glLineWidth(1.0f);
	}
	
	//draw numbers
	div_t coord;
	string str;
	stringstream ss2;
	for(int i=0;i<81;i++)
		ss2<<sudoku[i];
	str=ss2.str();
	for (int i=0; i<str.size(); i++)
	{
		coord=div(i,9);
		if(str.substr(i,1)!="0") //si l'élément est différent de 0 on l'affiche
		{
			if(sudoku_initial[i]!=0)
			{
				glColor3f(0.0,0.0,1.0);
				glRasterPos2i(coord.rem*10+5,coord.quot*10+5);
				glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, str.at(i));
			}
			else
			{
				glColor3f(0.0,0.0,0.0);
				glRasterPos2i(coord.rem*10+5,coord.quot*10+5);
				glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, str.at(i));
			}
		}
	}
	glutSwapBuffers();
}


void animate(int val)
{
	glutTimerFunc(TIMERMSECS,animate,0);
	draw();
}

void print_sudoku(const array<int,81> & sudoku_printed=sudoku)
{
	for (int i=0; i<81; i++)
		if (sudoku_printed[i]==0)
			cout << '.';
		else
			cout << sudoku_printed[i];
	cout << endl;
}

bool init_sudoku(string puzzle)
{
	if (puzzle.length()!=81)
	{
		// Wrong size!
		cout << "Error initializing sudoku: the input size is wrong." << endl;
		return false;
	}
	// Fill the sudoku array.
	int count = 0;
	string digit;
	for (int i=0; i<81; i++)
	{
		digit = puzzle.substr(i,1);
		if (digit!=".")
		{
			if ( !isdigit(puzzle.at(i)) )
			{
				cout << "Initialization data contains a character which is neither a digit not a dot at position " << i << " !" << endl;
				return false;
			}
			sudoku[i] = stoi(digit);
			++count;
		}
		else
			sudoku[i] = 0;
	}
	cout << "There are " << (81-count) << " empty cells to fill." << endl;
	cout << "Starting position:" << endl;
	print_sudoku();
	for(int i=0; i<27; i++)
	{
		set<int> cond;
		conditions[i] = cond;
	}
	sudoku_initial = sudoku;
	return true;
}

bool check_sudoku()
{
	//There are 27 conditions: 9 on rows + 9 on columns + 9 on sub-grids
	//The sudoku array will be scanned only once, with each element added to the 3 appropriate conditions, as a function of its index in the array.
	//Then the conditions will be checked: if all conditions have 9 elements and contain 9, then the condition is true.
	//This function returns true if the sudoku is solved, i.e. all 27 conditions are true.
	
	//Clear all the conditions before going through the elements.
	for (int i=0; i<27; i++)
		conditions[i].clear();
	//Conditions 0-8: rows
	//Conditions 9-17: columns
	//Conditions 18-27: sub-grids
	div_t d1;
	int gridx, gridy;
	for (int i=0; i<81; i++)
	{
		//Check individual digits are within range
		if ( sudoku[i]>9 || sudoku[i]<1 )
			return false;
		//Condition on rows
		d1 = div(i,9);
		conditions.at(d1.quot).insert(sudoku[i]);
		//Condition on columns
		conditions.at(d1.rem+9).insert(sudoku[i]);
		//Condition on grids
		gridy = div(i,27).quot;
		gridx = div(d1.rem,3).quot;
		conditions.at(gridy*3+gridx+18).insert(sudoku[i]);
	}
	for (int i=0; i<27; i++)
		if ( (conditions[i].size()!=9) || (conditions[i].count(9)!=1) || (conditions[i].count(0)==1) )
			return false;
	return true;
}

bool check_position(int pos,int candidate)
{
	//The 3 conditions associated with this position
	int idx_row, idx_col, idx_grid;
	div_t coord1 = div(pos,9);
	idx_row = coord1.quot;
	idx_col = coord1.rem;
	idx_grid = div(idx_row,3).quot + 3*div(idx_col,3).quot;
	set<int> c1,c2,c3;
	
	for (int i=0; i<81; i++)
	{
		if ( (i!=pos) && (sudoku[i]!=0) )
		{
			div_t d = div(i,9);
			if ( d.quot == idx_row )
				c1.insert(sudoku[i]);
			if ( d.rem == idx_col )
				c2.insert(sudoku[i]);
			if ( div(d.quot,3).quot+3*div(d.rem,3).quot == idx_grid )
				c3.insert(sudoku[i]);
		}
	}
	pair<set<int>::iterator,bool> ret1,ret2,ret3;
	//Add the candidate to the conditions
	ret1=c1.insert(candidate);
	ret2=c2.insert(candidate);
	ret3=c3.insert(candidate);
	
	if ( ret1.second && ret2.second && ret3.second )
		return true;
	return false;
}


bool solve(int pos)
{
	if (pos==81) {
		if (check_sudoku()) {
			std::cout << "Solution trouvée ! " << std::endl;
			print_sudoku();
			return true;
		}
	}
	else {
		if (sudoku_initial[pos]!=0)
			return solve(pos+1); //on va voir case suivante car celle-ci est fixée. Si la suivante est bonne on sort direct
		else {
			//il faut trouver la valeur de la case

			for (int i=1; i<=9; i++) {
				if (check_position(pos, i)) { //on vérifie s'il est possible de mettre i à la position pos
					sudoku[pos] = i;
					if(solve(pos+1))
						return true; //si la case plus loin renvoie true, on veut resortir immédiatement.	
				}
				// si on peut mettre aucune valeur c'est que la situation actuelle n'est pas réalisable et on doit revenir en arrière pour en tester une autre.
			}
			sudoku[pos] = 0; //on défait le choix
			return false;
		}
	}
	return true;
}


void solve_main()
{
	if(!solve(0))
		cout << "No solution found." << endl;
}

int main(int argc,char *argv[])
{
	string puzzle;
	if (argc<2)
		puzzle = string(81, '.');
	else
	{
		ifstream file (argv[1]);
		if (argc>=3)
		{
			string ignored;
			for (int i=1; i<atoi(argv[2]); i++)
				getline(file,ignored);
		}
		getline(file,puzzle);
	}
	if(!init_sudoku(puzzle))
		return 1;
	
	glutInit(&argc,argv);
	
	glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);
	
	glutInitWindowSize (700, 700);
	glutInitWindowPosition (200, 200);
	
	glutCreateWindow ("Sudoku");
	
	init(255.0,255.0,255.0);
	
	thread t(solve_main);
	
	glutTimerFunc(TIMERMSECS,animate,0);
	glutDisplayFunc(draw);
	glutMainLoop();
	t.join();
	
	return 0;
}
