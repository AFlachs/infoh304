#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <algorithm>
#include <chrono>
#include <random>
#include <iterator>

#include "src/quicksort.h"
#include "src/mergesort.h"

using namespace std;
using namespace std::chrono;

typedef high_resolution_clock clk;


int check_sorted(vector<string> & l1, vector<string> & l2)
{
	int identical=0;
	for(int i=0; i<l1.size(); i++)
	{
		if( l1[i].compare(l2[i]) == 0 )
			identical++;      
	}
	cout << "Similarity between the two sorted lists: " << (identical/l1.size())*100 << "%" << endl;
	return (int)(identical/l1.size())*100;
}

vector<unsigned long long> compare_algorithms(vector<string> & l_merge, vector<string> & l_stl, vector<string> & l_quick)
{
		vector<unsigned long long> result;
		nanoseconds time_quick, time_stl, time_merge;
		clk::time_point start, stop;
		for( int i=0; i<100000; i++)
		{
			start = clk::now();
			stop = clk::now();
		}
		//cout << "Element count: " << l_merge.size() << endl;
		start = clk::now();
		my_quicksort(l_quick, 0, l_quick.size()-1);
		stop = clk::now();
		time_quick = duration_cast<nanoseconds>(stop-start);
		//cout << "Elapsed time for quick sort: " << (long)time1.count() << " nanoseconds" << endl;

		start = clk::now();
		sort(l_stl.begin(), l_stl.end());
		stop = clk::now();
		time_stl = duration_cast<nanoseconds>(stop-start);
		//cout << "Elapsed time for STL sort: " << (long)time2.count() << " nanoseconds" << endl;
;
		start = clk::now();
		mergesort(l_merge, 0, l_merge.size()-1);
		stop = clk::now();
		time_merge = duration_cast<nanoseconds>(stop-start);
		//cout << "Elapsed time for merge sort: " << (long)time2.count() << " nanoseconds" << endl;

		check_sorted(l_merge,l_stl);
		check_sorted(l_quick, l_stl);
		
		result.push_back((unsigned long long)l_merge.size());
		result.push_back(time_quick.count());
		result.push_back(time_merge.count());
		result.push_back(time_stl.count());
		return result;

}

int main(int argc, char *argv[])
{
	string word;
	
	vector<string> l, l1, l2, l3; // to get the words, l gets all and the rest gets a random distr
	vector<unsigned long long> result; // the time results
	
	if ( argc!=3 )
	{
		cout << "Enter a file name and the maximum number of elements." << endl;
	}
	else
	{
		int max = atoi(argv[2]);
		ifstream file (argv[1]); //reading file given as first parameter
		
		const double resolution = double(clk::period::num) / double(clk::period::den);
		cout << "System clock info:" << endl;
		cout << "Clock period: "<< resolution*1e9 << " nanoseconds." << endl;
		ofstream data ("plot.dat"); //to plot the results
		data << "#Element size\t#Quick Sort\t#Merge Sort\t#STL Sort\t" << endl;
		string str;
		while ( file >> word )
		{
			l.push_back(word); //adding every word from file
		}
		string temp;
		int steps = 1;
		int base = 2;
		random_device rd;
		mt19937 gen(rd());
		uniform_int_distribution<int> r(0, l.size()); //random distr of l
		while ( l.size() > (int)pow(base,steps) && ((int)pow(base,steps) <= max) && ((int)pow(base,steps) < l.size()) )
		{
			for ( int i=1; i<=(int)pow(base,steps); i++ )
			{
			// random distribution between the three lists
				temp = l[r(gen)];
				l1.push_back(temp);
				l2.push_back(temp);
				l3.push_back(temp);
			}
			result = compare_algorithms(l1,l2,l3); // comparer les algo en les utilisant
			for ( int i=0; i<result.size(); i++)
			{
				data << result[i];
				data << "\t";
			}
			data << endl;
			
			l1.clear();
			l2.clear();
			l3.clear();
			result.clear();
			steps++;
		} 
		file.close();
		data.close();
	}
	return 0;
}

