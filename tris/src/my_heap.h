#ifndef MY_HEAP
#define MY_HEAP

#include <vector>
#include <cmath>
#include <iostream>
#include "swap.h"


/*
    Ne fonctionne pas encore, faut tester si le buildheap fonctionne
*/
template <typename T>
class My_heap
{
private:
    std::vector<T, std::allocator<T>> data;
    void maxHeapify(int i);

    int lsize;

    int parent(int i) const {
        return floor((float)i/2);
    }
    int left(int i) const {
        return 2*i;
    }
    int right(int i) const {
        return 2*i+1;
    } 
    int height(int i) const {
        return floor(log(lsize/i));
    }

public:
    My_heap(std::vector<T> & A = {});

    T& operator[](int i);

    std::vector<T> & getData() {return data;}
    int extractMaximum();

    void sort();

};

template <typename T>
void My_heap<T>::maxHeapify(int i) {
    int l = left(i);
    int r = right(i);
    int largest = i;
    if (l<= lsize && operator[](l) > operator[](i))
        largest = l;
    if (r<= lsize && operator[](r) > operator[](largest))
        largest = r;
    if (largest != i) {
        swap(data, i, largest);
        maxHeapify(largest);
    }
}


template <typename T>
My_heap<T>::My_heap(std::vector<T, std::allocator<T>> & A) {
    data = A;
    lsize = A.size();
    for (int i = floor((float)lsize/2); i>0; --i)
        maxHeapify(i);

    for (int i=0; i<data.size(); i++) {
        std::cout << data[i] << std::endl;
    }
}

template <typename T>
T& My_heap<T>::operator[](int i) {
    return data.operator[](i-1);
}

template <typename T>
int My_heap<T>::extractMaximum() {
    swap(data, 1, lsize);
    int max = operator[](lsize);
    lsize -= 1;
    maxHeapify(1);
    return max;
}

template <typename T>
void My_heap<T>::sort(){
    for (int i=lsize; i>2; --i) {
        extractMaximum();
    }
    lsize = data.size(); // reset lsize to the correct value
}
#endif