#ifndef SWAP_H
#define SWAP_H
#include <vector>

template <typename T, typename Alloc>
void swap(std::vector<T, Alloc> & A, int i, int j) {
    /**
     * Echange les éléments aux positions i et j du tableau A
    **/
    T temp = A[i];
    A[i] = A[j];
    A[j] = temp;
}

#endif