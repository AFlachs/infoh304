
#include <random>
#include <vector>

#include "swap.h"
#include "insertion.h"


template<typename T, typename Alloc>
int partition(std::vector<T, Alloc> &A, int left, int right) {
    /**
     * Partitionne le tableau A en 3 parties : 
     *      un pivot (élément le plus à droite au départ)
     *      Une partie à gauche du pivot tq tous les éléments sont plus petits que le pivot
     *      Une partie à droite du pivot tq tous les éléments sont plus grands que le pivot
     * Attention : Vérifier que right ne cause pas de segfault -> à faire hors de la fonction.
     * @return : position finale du pivot
     */
    T pivot = A[right];
    int i = left-1;
    for (int j=left; j<right; j++) { // parcourt les éléments du tableau et les place à gauche si plus petit que le pivot
        if (A[j] <= pivot) {
            ++i;
            swap(A, i, j);
        }
    }
    swap(A, i+1, right);
    return i+1;

}

template<typename T, typename Alloc>
int random_partition(std::vector<T, Alloc> & A, int left, int right) {
    // Partitionne à partir d'un pivot aléatoire
    srand(time(NULL));
    int i = rand() % (right-left +1) + left; // Nombre aléatoire entre left et right
    swap(A, i, right);
    return partition(A, left, right);
}

template<typename T, typename Alloc>
void my_quicksort(std::vector<T, Alloc> & A, int left, int right) {
    if (left<right) {
        if (right - left < 15) {
            my_insertion_sort(A, left, right);
        }
        int mid = random_partition(A, left, right);
        my_quicksort(A, left, mid-1);
        my_quicksort(A, mid+1, right);
    }
}