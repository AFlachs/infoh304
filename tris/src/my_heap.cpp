#include "my_heap.h"

template <typename T>
void My_heap<T>::maxHeapify(int i) {
    int l = left(i);
    int r = right(i);
    int largest = i;
    if (l<= lsize && data[l] > data[i])
        largest = l;
    if (r<= lsize && data[r] > data[largest])
        largest = r;
    if (largest != i) {
        swap(data, i, largest);
        maxHeapify(largest);
    }
}


template <typename T>
My_heap<T>::My_heap(std::vector<T, std::allocator<T>> & A) {
    data = A;
    for (int i = floor((float)lsize/2); i>0; --i)
        maxHeapify(i);
}

template <typename T>
T& My_heap<T>::operator[](int i) const {
    return data(i-1);
}
template <typename T>
int My_heap<T>::extractMaximum() {
    swap(data, 1, lsize);
    int max = data[data.size()];
    lsize -= 1;
    return max;
}

template <typename T>
void My_heap<T>::sort(){
    for (int i=lsize; i>2; --i) {
        extractMaximum();
    }
    lsize = data.size(); // reset lsize to the correct value
}