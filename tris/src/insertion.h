#ifndef MY_INSERTION_H
#define MY_INSERTION_H

#include "swap.h"
#include <vector>
template <typename T>
void my_insertion_sort(std::vector<T> & l, int left=0, int right=-1)
{
	/*
	 *	Sorts the vector l between the two elements left and right (included)
	 */
	if (right<0){
		right = l.size()-1;
	}
	for(int i=left; i<=right; i++)
	{
		// looping through the interesting part of l
		int j=i;
		while ( j>0 && l[j-1]>l[j] ) //the element before j is higher -> we switch 
		{
            swap(l, j, j-1);
			j--; //this line implies that we check for every value between 0 and i while j decreases
		}
	}
}


#endif