#include <vector>
#include "insertion.h"

template<typename T>
void my_merge(std::vector<T> & A, int left, int mid, int right) {
    int nL = mid-left, nR = right-mid+1; // nombre d'éléments dans les sous tableaux de gauche et droite
    std::vector<T> L, R;

    for (int i=left; i<mid; i++) { L.push_back(A[i]); }
    for (int i=mid; i <= right; i++) { R.push_back(A[i]); }
    int iL=0, iR=0, k;

    for (k=left; k<=right && iL < nL && iR < nR; k++) {
        // On s'arrête lorsqu'on est au bout d'un vecteur ou qu'on a traversé tout le tableau
        if (L[iL] <= R[iR]) {
            A[k] = L[iL];
            iL++;
        }
        else {
            A[k] = R[iR];
            iR++;
        }
    }
    // Il faut maintenant terminer le tableau qui n'a pas été parcouru en entier
    for (iL = iL; iL < nL; iL++) {
        A[k] = L[iL];
        k++;
    }

    for (iR = iR; iR < nR; iR++) {
        A[k] = R[iR];
        k++;
    }
}

template<typename T>
void mergesort(std::vector<T> & A, int left, int right) {
    if (left>= right) {return;}

    if (right - left < 15) {
        my_insertion_sort(A, left, right);
    }
    int mid = ceil((float)(left+right)/2);
    mergesort(A, mid, right);
    mergesort(A, left, mid-1);
    my_merge(A, left, mid, right);
}
