#include <random>
#include <stdio.h>
#include <iostream>
#include "src/quicksort.h"
#include "src/my_heap.h"
#include "src/mergesort.h"
#include <vector>

template<typename T, typename Alloc>
bool isSorted(std::vector<T, Alloc>& A, int left, int right){
    for (int i=left; i<right-1; ++i) {
        if (A[i] > A[i+1]) {
            return false;
        }
    }
    return true;
}


int main(int argc, char* argv[]) {
    size_t size = 10000;
    int maxValue = 1000000; // valeur max des éléments du tableau.

    std::vector<int> testArray(size);
    for (size_t i=0; i<size; i++) {
        testArray[i] = rand() % maxValue; // Nombre aléatoire entre left et right
    }


    /*
    std::cout << "Starting quicksort" << std::endl;
    my_quicksort(testArray, 0, size);
    std::cout << "Ended quicksort" << std::endl;
    */
    std::vector<int> startArray = testArray;
    mergesort(testArray, 0, testArray.size()-1);

    if (startArray == testArray) {std::cout << "no change in array" << std::endl;}

    if (isSorted(testArray, 0, size)) {
        std::cout << "Tableau bien trié" << std::endl;
    }
    else {
        std::cout << "Tableau non trié" << std::endl;
    }
    return 0;
}