#include <stdio.h>
#define LONGMAX 30
int main()
{
	char c, mot[LONGMAX+1];
	int i;
	for ( i=0; i<LONGMAX; i++ )
	{
		c = getchar();
		if ( c>='a' && c<='z' )
			mot[i] = c;
		else
			break;
	}
	mot[i] = '\0';
	printf("Le mot %s est de longueur %i\n", mot, i);
	return 0;
}