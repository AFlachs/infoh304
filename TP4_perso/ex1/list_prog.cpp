#include "list.h"

int main()
{
	Liste l;
	l.insereTrie("Un");
	l.insereTrie("Deux");
	l.insereTrie("Trois");
	l.insereTrie("Quarante"); //milieu de liste
	l.insereTrie("Zebre"); //fin de liste
	l.insereTrie("Abracadabra"); //insère en début de liste
	l.insereTrie("Un");
	l.imprimeListe();
	l.supprime();
	if(l.getTete() == NULL)
		std::cout << "Il n'y a plus de tete" << std::endl;
	return 0;
}
