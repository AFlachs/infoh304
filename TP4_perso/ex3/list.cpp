#include "list.h"
#include <ostream>
using std::cout;
using std::endl;

Liste::Liste()
{
	nombreNoeuds = 0;
	tete = NULL;
}

Liste::~Liste()
{
	//Destructeur redéfini pour détruire tous les noeuds de la liste. 
	while(tete!=NULL)
		supprime();
}

void Liste::insere(string chaine)
{
	// Insère un nouveau noeud en tete de liste
	nombreNoeuds++;
	Noeud* n=new Noeud(chaine);
	n->setSuivant(tete);
	tete = n;
	n->incrementCompteur();
}

void Liste::insereTrie(string chaine){
	/*
	 * Insère un élément donné dans la liste à l'endroit adéquat, il faut 
	 * faire attention aux différents cas envisageables :
	 * 1. L'élément doit s'insérer au milieu de la liste
	 * 2. L'élément doit s'insérer au début de la liste 
	 * 3. L'élément doit s'insérer à la fin de la liste 
	 * 4. Insertion dans une liste vide
	 * 5. Si le mot est déjà dedans on n'insère pas
	*/
	Noeud* current = tete;
	if (tete == NULL || (chaine < current->getData())){
		// current->getData() ne plante pas si tete est NULL pcq le ||
		// regarde d'abord si le membre de gauche est vrai, s'il est deja faux
		// il ne va pas chercher plus loin.
		insere(chaine);
	}
	else {
		while ((current->getSuivant() != NULL) 
				&& (current->getSuivant()->getData() <= chaine)) 
			// le data du suivant est avant la chaine -> on place 
			// d'office après le suivant (pas forcément juste après)
			current = current->getSuivant();
		// Arrivé ici la chaine est à placer après le mot actuel,
		// on fait un équivalent de insere mais on vérfie que chaine n'est pas
		// identique au mot précédent
		if (chaine != current->getData())
		{
			nombreNoeuds++;
			Noeud* n = new Noeud(chaine);
			n->setSuivant(current->getSuivant());
			current->setSuivant(n);
			n->incrementCompteur();
			return; //on casse la boucle puisqu'on a inséré le mot
		}
		current->incrementCompteur();
	}
	
	return;
}

void Liste::supprime()
{
	if (tete != NULL)
	{
		Noeud* temp = tete; // noeud temporaire créé sur le tas
		tete = tete->getSuivant();
		delete temp;
		nombreNoeuds--;
	}
	else
		cout << "La liste est déjà vide !" << endl;
}

void Liste::imprimeListe(std::ostream & fichierOut) const
{
	Noeud* nextNoeud = tete;
	while(nextNoeud != NULL)
	{
		fichierOut << nextNoeud->getData() << " présent " << nextNoeud->getCompteur() 
			<< " fois." << endl;
		nextNoeud = nextNoeud->getSuivant();
	}
}

int Liste::getNombreNoeuds() const
{
	return nombreNoeuds;
}

Noeud* Liste::getTete() const
{
	return tete;
}
