#include "noeud.h"
#include <fstream>
#include <iostream>

class Liste
{
private:
	int nombreNoeuds;
	Noeud* tete;
public:
	Liste();
	~Liste();
	
	void insere(string chaine);
	void insereTrie(string chaine);
	// Attention sans doublon dans la liste
	void supprime();
	void imprimeListe(std::ostream & fichierOut=std::cout) const;
	int getNombreNoeuds() const;
	Noeud* getTete() const;
};
