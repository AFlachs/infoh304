#include "list.h"
using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;


int main(int argc, char *argv[])
{
	ifstream fichierIn (argv[1]); // on ouvre le fichier passé en arg
	Liste liste;
	if (fichierIn.is_open()){
		string mot;
		while (	fichierIn >> mot ) 
			liste.insereTrie(mot);
		fichierIn.close();
	}
	else {
		cout <<	"Impossible d'ouvrir le fichier" << endl;
		return 1;
	}
	
	// On écrit dans un fichier la liste trouvée.
	ofstream fichierOut (argv[2]);
	if (fichierOut.is_open())
		liste.imprimeListe(fichierOut);	
	else {
		cout << "Impossible d'écrire dans le fichier" << endl;
	}
	return 0;
}
