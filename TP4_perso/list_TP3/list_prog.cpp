#include "list.h"

int main()
{
	Liste l;
	l.insere("Un");
	l.insere("Deux");
	l.imprimeListe();
	std::cout<< l.getNombreNoeuds() <<std::endl;
	
	l.supprime();
	l.insere("Trois");
	l.imprimeListe();
	l.supprime();
	if(l.getTete() == NULL)
		std::cout << "Il n'y a plus de tete" << std::endl;

	return 0;
}
