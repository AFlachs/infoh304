#include "list.h"
using std::cout;
using std::endl;

Liste::Liste()
{
	nombreNoeuds = 0;
	tete = NULL;
}

Liste::~Liste()
{
	//Destructeur redéfini pour détruire tous les noeuds de la liste. 
	while(tete!=NULL)
		supprime();
}

void Liste::insere(string chaine)
{
	nombreNoeuds++;
	Noeud* n=new Noeud(chaine);
	n->setSuivant(tete);
	tete = n;
}

void Liste::supprime()
{
	if (tete != NULL)
	{
		Noeud* temp = tete; // noeud temporaire créé sur le tas
		tete = tete->getSuivant();
		delete temp;
		nombreNoeuds--;
	}
	else
		cout << "La liste est déjà vide !" << endl;
}

void Liste::imprimeListe() const
{
	Noeud* nextNoeud = tete;
	while(nextNoeud != NULL)
	{
		cout << nextNoeud->getData() << endl;
		nextNoeud = nextNoeud->getSuivant();
	}
}

int Liste::getNombreNoeuds() const
{
	return nombreNoeuds;
}

Noeud* Liste::getTete() const
{
	return tete;
}
