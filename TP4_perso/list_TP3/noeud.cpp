#include "noeud.h"

Noeud::Noeud() //on précise que la foncion noeud vient de la classe noeud
{
	suivant = NULL;
	data = ""; //par défaut cpp alloue la mémoire sans l'initialiser
	// du coup bonne pratique de l'initialiser avec contenu vide pour pas
	// avoir de problèmes
	// la classe std::string permet la manipulation des strings comme objets,
	// on n'a pas les soucis qu'on avait avec les tableaux de char du C.
}

Noeud::Noeud(string chaine) 
{
	suivant = NULL;
	data = chaine;
}

void Noeud::setSuivant(Noeud* n)
{
	suivant = n;
}

Noeud* Noeud::getSuivant() const //le const à la fin indique que la fonction
	//ne va pas modifier l'objet
{
	return suivant;
}

string Noeud::getData() const
{
	return data;
}
