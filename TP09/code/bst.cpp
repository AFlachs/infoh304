#include <iostream>
#include <string>
#include <chrono>
#include <thread>
#include <random>
#include <unordered_set>
extern "C" {
#include "UbigraphAPI.h"
}

using namespace std;

class BinarySearchTree
{
private:
	struct node
	{
		node *left;
		node *right;
		node *parent;
		int content;
	};
	node* root;
	node* tree_search(int content);
	node* tree_search(int content, node* location);
	void delete_no_child(node* location);
	void delete_left_child(node* location);
	void delete_right_child(node* location);
	void delete_two_children(node* location);
	
public:
	BinarySearchTree ()
	{
		root=NULL;
	}
	bool isEmpty()
	{
		return root==NULL;
	}
	void insert_element(int content);
	void delete_element(int content);
	void inorder(node* location);
	void print_inorder();
};


void BinarySearchTree::insert_element(int content)
{
	//New node
	node* n = new node();
	n->content = content;
	n->left = NULL;
	n->right = NULL;
	n->parent = NULL;
	
	//For visualization
	int eid,vid;
	this_thread::sleep_for(chrono::milliseconds(100));
	ubigraph_new_vertex_w_id(content);
	ubigraph_set_vertex_attribute(content, "color", "#0000ff");
	ubigraph_set_vertex_attribute(content, "label", to_string(content).c_str());
	
	if(isEmpty())
	{
		root = n;
		ubigraph_set_vertex_attribute(content, "color", "#ff0000");
		this_thread::sleep_for(chrono::milliseconds(100));
		ubigraph_set_vertex_attribute(content, "color", "#0000ff");
		this_thread::sleep_for(chrono::milliseconds(100));
	}
	else
	{
		
		node* pointer = root;
		
		while ( pointer != NULL )
		{
			n->parent = pointer;
			
			if(n->content > pointer->content)
			{
				vid = pointer->content;
				ubigraph_set_vertex_attribute(vid, "color", "#ff0000");
				this_thread::sleep_for(chrono::milliseconds(100));
				ubigraph_set_vertex_attribute(vid, "color", "#0000ff");
				this_thread::sleep_for(chrono::milliseconds(100));
				
				pointer = pointer->right;
				
			}
			else
			{
				vid = pointer->content;
				ubigraph_set_vertex_attribute(vid, "color", "#ff0000");
				this_thread::sleep_for(chrono::milliseconds(100));
				ubigraph_set_vertex_attribute(vid, "color", "#0000ff");
				this_thread::sleep_for(chrono::milliseconds(100));
				
				pointer = pointer->left;
				
			}
		}
		
		if ( n->content < n->parent->content )
		{
			n->parent->left = n;
			this_thread::sleep_for(chrono::milliseconds(200));
			eid = ubigraph_new_edge(n->parent->content, content);
			ubigraph_set_edge_attribute(eid, "oriented", "true");
			
		}
		else
		{
			n->parent->right = n;
			this_thread::sleep_for(chrono::milliseconds(200));
			eid = ubigraph_new_edge(n->parent->content, content);
			ubigraph_set_edge_attribute(eid, "oriented", "true");		
		}
	}
}

void BinarySearchTree::inorder(node* location)
{
  if (location != NULL) {
    inorder(location->left);
    std::cout << location->content << " ";
    inorder(location->right);
  }	
}

void BinarySearchTree::print_inorder()
{
	cout << "Contenu de l'arbre :";
	inorder(root);
	cout << endl;
}

BinarySearchTree::node* BinarySearchTree::tree_search(int content)
{
	return tree_search(content, root);
}

BinarySearchTree::node* BinarySearchTree::tree_search(int content, node* location)
{
  /*
  if (location != NULL) {
    if (location->content == content){
      return location;
    }
    else if (location->content < content) {
      // La clé du noeud actuelle est plus petite celle recherchée -> on va à droite
      tree_search(content, location->right);
    }
    else {
      // La clé actuelle est plus grande que celle recherchée -> on va à gauche
      tree_search(content, location->left);
    }
  }
	return NULL;
  */
  while (location != NULL) {
    if (location->content == content){
      return location;
    }
    else if (location->content < content) {
      // La clé du noeud actuelle est plus petite celle recherchée -> on va à droite
      location = location->right;
    }
    else {
      // La clé actuelle est plus grande que celle recherchée -> on va à gauche
      location = location->left;
    }
  }
	return NULL;
  
}

void BinarySearchTree::delete_no_child(node* location)
{
  if (location->parent->left == location)
    location->parent->left=NULL;
  else
    location->parent->right=NULL;
  delete location;
}

void BinarySearchTree::delete_left_child(node* location)
{
    // On commence par vérifier si le noeud est un enfant de gauche ou de droite
    if (location->parent->left == location) {
      //enfant de gauche
      location->parent->left = location->left;
    }
    else
      location->parent->right = location->left;
    delete location;
    return;
	
}

void BinarySearchTree::delete_right_child(node* location)
{
    // On commence par vérifier si le noeud est un enfant de gauche ou de droite
    if (location->parent->left == location) {
      //enfant de gauche
      location->parent->left = location->right;
    }
    else
      location->parent->right = location->right;
    delete location;
    return;
	
}

void BinarySearchTree::delete_two_children(node* location)
{
  // Le noeud à supprimer a deux enfants, il doit être remplacé par son successeur (le plus petit élément de son arbre de droite puisqu'on est sûr que le noeud a un enfant à droite)
  node* successor = location->right; // on sait que ce pointeur n'est pas null
  while (successor->left != NULL) {
    successor = successor->left; //on part du sous-arbre de droite et on va le plus à gauche possible
  }
  // le successeur ne peut pas avoir d'enfant à gauche, on vérifie s'il en a un a droite, si c'est le cas "supprime" le successeur à cet endroit là
  if (successor->right != NULL) {
    successor->right->parent = successor->parent;
    if (successor->parent->left != NULL && successor->parent->left == NULL )
      successor->parent->left = successor->right;
    else
      successor->parent->right = successor->right;
  }
  else {
    successor->parent->left = NULL;
  }

  // Finalement on remplace location par successor pour le parent
  if (location->parent->left == location) {
    location->parent->left = successor;
  }
  else {
    location->parent->right = successor;
  }

  // Et on refixe les enfants correctement
  if (location->left != NULL)
    successor->left = location->left;
  if (location->right != NULL)
    successor->right = location->right;

}

void BinarySearchTree::delete_element(int content)
{
  node* toDel;
  toDel = tree_search(content);
  if (toDel == NULL) {
    cout << "L'élément à supprimer n'a pas été trouvé" << endl;
    return;
  }
  // En fonction du nombre d'enfants le traitement n'est pas le même
  cout << "suppression de l'élément de clé " << toDel->content << endl;
  if (toDel->left == NULL && toDel->right == NULL) {
    cout << "Cet élément est une feuille" << endl;
    // Pas d'enfants -> feuille
    delete_no_child(toDel);
  }
  else if (toDel->left == NULL || toDel->right== NULL) {
    // Impossible de ne pas avoir d'enfant ici -> un seul enfant si l'un des deux est null
    if (toDel->left == NULL)
      delete_right_child(toDel);
    else
      delete_left_child(toDel);
  }
  else {
    // Ici forcément deux enfants
    delete_two_children(toDel);
  }
}


int main()
{
	ubigraph_clear();
	unordered_set<int> vertices;
	BinarySearchTree bst;
	
	random_device rd;
	mt19937 gen(rd());
	uniform_int_distribution<int> r(0, 1000);
	
  bst.insert_element(14);
  bst.insert_element(12);
  bst.insert_element(17);
  bst.insert_element(11);
  bst.insert_element(13);
  bst.insert_element(16);
  bst.insert_element(19);
  bst.insert_element(18);
  bst.insert_element(10);
  bst.insert_element(15);

  bst.delete_element(15);
  bst.delete_element(14);
  bst.delete_element(11);
  bst.print_inorder();

	
	return 0;
}
