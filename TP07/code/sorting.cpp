#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <algorithm>
#include <chrono>
#include <random>
#include <iterator>

using namespace std;
using namespace std::chrono;

typedef high_resolution_clock clk;

void insertion_sort(vector<string> & l, int left, int right); //declaring


void merge(vector<string> & l, int left, int mid, int right)
{
	/** Merge two sorted vectors 
	 *	
	 *	l : Starting vector divided in two subvecs between left, mid
	 *		and mid+1, right
	 *
	 * **/

	// creating subvectors
	vector<string> subLeft;
	vector<string> subRight;

	// Copie efficace : 
	//subLeft.insert(subLeft.begin(), l.begin(), l.begin()+mid);
	//subRight.insert(subRight.begin(), l.begin()+mid, l.begin()+right+1);
	for (int i = left; i<mid ; i++) //copying from left to mid-1 into subLeft
		subLeft.push_back(l[i]);

	for (int i = mid; i <= right; i++) //copying from mid to right into subRight
		subRight.push_back(l[i]);
	// subleft and subRight are the vectors to merge
	subLeft.push_back("zzzzzz"); //infinite value to stop merging
	subRight.push_back("zzzzzz"); // any character coming after letters in ascii will do

	// merging
	int iL =0, iR=0;
	for (int i=left; i<= right; i++){ //looping as many times as there are elements to sort
		if (subLeft[iL] <= subRight[iR])
		{
			l[i] = subLeft[iL];
			++iL;
		}
		else {
			l[i] = subRight[iR];
			iR++;
		}
	}
}

void merge_sort(vector<string> & l, int left, int right)
{
	/** Sort a vector by calling merge() recursively 
	 *
	 *	l: to sort
	 *	left : starting point
	 *	right : ending point
	 *
	 *	If the vector contains less that ~150 elements an insertion sort is more efficient
	 * **/
	int mid;

	if (right - left < 150) {
		insertion_sort(l, left, right);
	}
	else if (left < right) {
		mid = ceil((float) (left+right)/2 ); 
		merge_sort(l, mid, right);
		merge_sort(l, left, mid-1);
		merge(l, left, mid, right);
	}

}

void insertion_sort(vector<string> & l, int left=0, int right=-1)
{
	/*
	 *	Sorts the vector l between the two elements left and right (included)
	 */
	if (right<0){
		right = l.size()-1;
	}
	string tmp;
	for(int i=left; i<=right; i++)
	{
		// looping through the interesting part of l
		int j=i;
		while ( j>0 && l[j-1]>l[j] ) //the element before j is higher -> we switch 
		{
			tmp = l[j]; //memorizing element
			l[j] = l[j-1];
			l[j-1] = tmp;
			j--; //this line implies that we check for every value between 0 and i while j decreases
		}
	}
}


int check_sorted(vector<string> & l1, vector<string> & l2)
{
	int identical=0;
	for(int i=0; i<l1.size(); i++)
	{
		if( l1[i].compare(l2[i]) == 0 )
			identical++;      
	}
	cout << "Similarity between the two sorted lists: " << (identical/l1.size())*100 << "%" << endl;
	return (int)(identical/l1.size())*100;
}

vector<unsigned long long> compare_algorithms(vector<string> & l_merge, vector<string> & l_stl, vector<string> & l_insertion)
{
		vector<unsigned long long> result;
		nanoseconds time1, time2, time3;
		clk::time_point start, stop;
		for( int i=0; i<100000; i++)
		{
			start = clk::now();
			stop = clk::now();
		}
		//cout << "Element count: " << l_merge.size() << endl;
		start = clk::now();
		merge_sort(l_merge,0,l_merge.size()-1);
		stop = clk::now();
		time1 = duration_cast<nanoseconds>(stop-start);
		//cout << "Elapsed time for merge sort: " << (long)time1.count() << " nanoseconds" << endl;
		start = clk::now();
		sort(l_stl.begin(), l_stl.end());
		stop = clk::now();
		time2 = duration_cast<nanoseconds>(stop-start);
		//cout << "Elapsed time for STL sort: " << (long)time2.count() << " nanoseconds" << endl;
		start = clk::now();
		insertion_sort(l_insertion);
		stop = clk::now();
		time3 = duration_cast<nanoseconds>(stop-start);
		//cout << "Elapsed time for insertion sort: " << (long)time3.count() << " nanoseconds" << endl;
		check_sorted(l_merge,l_stl);
		
		result.push_back((unsigned long long)l_merge.size());
		result.push_back(time1.count());
		result.push_back(time2.count());
		result.push_back(time3.count());
		return result;

}

int main(int argc, char *argv[])
{
	string word;
	
	vector<string> l, l1, l2, l3; // to get the words, l gets all and the rest gets a random distr
	vector<unsigned long long> result; // the time results
	
	if ( argc!=3 )
	{
		cout << "Enter a file name and the maximum number of elements." << endl;
	}
	else
	{
		int max = atoi(argv[2]);
		ifstream file (argv[1]); //reading file given as first parameter
		
		const double resolution = double(clk::period::num) / double(clk::period::den);
		cout << "System clock info:" << endl;
		cout << "Clock period: "<< resolution*1e9 << " nanoseconds." << endl;
		ofstream data ("plot.dat"); //to plot the results
		data << "#Element size\t#Merge Sort\t#STL Sort\t#Insertion Sort" << endl;
		string str;
		while ( file >> word )
		{
			l.push_back(word); //adding every word from file
		}
		string temp;
		int steps = 1;
		int base = 2;
		random_device rd;
		mt19937 gen(rd());
		uniform_int_distribution<int> r(0, l.size()); //random distr of l
		while ( l.size() > (int)pow(base,steps) && ((int)pow(base,steps) <= max) && ((int)pow(base,steps) < l.size()) )
		{
			for ( int i=1; i<=(int)pow(base,steps); i++ )
			{
			// random distribution between the three lists
				temp = l[r(gen)];
				l1.push_back(temp);
				l2.push_back(temp);
				l3.push_back(temp);
			}
			result = compare_algorithms(l1,l2,l3); // comparer les algo en les utilisant
			for ( int i=0; i<result.size(); i++)
			{
				data << result[i];
				data << "\t";
			}
			data << endl;
			
			l1.clear();
			l2.clear();
			l3.clear();
			result.clear();
			steps++;
		} 
		file.close();
		data.close();
	}
	return 0;
}

