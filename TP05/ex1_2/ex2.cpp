#include <iostream>
#include <string>
#include <map>
#include <fstream>
using namespace std;

void printMap(const map<string, int> & c, ostream & out = cout);

int main(int argc, char *argv[])
{
	pair<map<string, int>::iterator, bool> ret;
	map<string, int> wordMap;
	int wordNb;

	ifstream fichierIn (argv[1]);

	if (fichierIn.is_open()){
		string mot;
		while (fichierIn >> mot)
		{
			// on n'a pas besoin de la valeur de retour
			ret = wordMap.insert(pair<string, int>(mot, 1));
			if (!ret.second)
				(ret.first)->second++;
		}
		fichierIn.close();
		wordNb = wordMap.size();
	}
	else {
		cout << "Impossible d'ouvrir le fichier" << endl;
		return 1;
	}


	// On écrit dans un fichier la liste trouvée.
	ofstream fichierOut (argv[2]);
	if (fichierOut.is_open())
	{
		cout << "Ecriture du fichier lancée." << endl;
		printMap(wordMap, fichierOut);
		cout << "Ecriture du fichier terminée." << endl;
	}
	else {
		cout << "Impossible d'écrire dans le fichier" << endl;
	}

	cout << "Le dictionnaire contient " << wordNb << " mots." << endl;

	return 0;
}


// Ce print est identique à celui du template vector vu dans le cours 4.
void printMap(const map<string, int> & c, ostream & out){
	map<string, int>::const_iterator itr;
	// un itérateur est comme un pointeur pour une struc de données
	for (itr = c.begin(); itr != c.end(); ++itr)
		out << itr->first << ", " << itr->second << endl;
}
