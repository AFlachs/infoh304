#include <iostream>
#include <string>
#include <set>
#include <fstream>
using namespace std;

void printSet(const set<string> & c, ostream & out = cout);

int main(int argc, char *argv[])
{
	pair<set<string>::iterator, bool> ret;
	set<string> wordSet;
	int wordNb;

	ifstream fichierIn (argv[1]);

	if (fichierIn.is_open()){
		string mot;
		while (fichierIn >> mot)
			// on n'a pas besoin de la valeur de retour
			wordSet.insert(mot);
		fichierIn.close();
		wordNb = wordSet.size();
	}
	else {
		cout << "Impossible d'ouvrir le fichier" << endl;
		return 1;
	}
	printSet(wordSet);
	cout << "Le livre contient " << wordNb << " mots." << endl;

}


// Ce print est identique à celui du template vector vu dans le cours 4.
void printSet(const set<string> & c, ostream & out){
	set<string>::const_iterator itr;
	// un itérateur est comme un pointeur pour une struc de données
	for (itr = c.begin(); itr != c.end(); ++itr)
		out << *itr << endl;
}
